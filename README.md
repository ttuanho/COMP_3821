# COMP_3821: Extended Algorithms and Programming Techniques

### Introduction
Correctness and efficiency of algorithms. Computational complexity: time and space bounds. Techniques for best-case, worst-case and average-case time and space analysis. Designing algorithms using induction, divide-and-conquer and greedy strategies. Algorithms: sorting and order statistics, trees, graphs, matrices. Intractability: classes P, NP, and NP-completeness, approximation algorithms. (See UNSW handbook for [COMP 3121](https://www.handbook.unsw.edu.au/undergraduate/courses/2019/COMP3121/?q=comp3121&ct=all))

### Resources

* COMP 3**1**21 in previous years:
    - COMP 3121 lectures & assignment notest by [kateee1966](https://github.com/kateee1966/comp3121)